package tests;

import org.junit.jupiter.api.Test;

import concretes.MineSweeper;

public class MineSweeperTest {
	@Test
	public void test() {
		MineSweeper mine = new MineSweeper(10,10);
		mine.generateMinesLocation(20);
		mine.setBoard();
		mine.tab[2][2] = 0;
		mine.tabMine[2][2] = 0;
		mine.tabRevealed[2][2] = true;
		mine.tab[2][3] = 0;
		mine.tabMine[2][3] = 0;
		mine.tabRevealed[2][3] = true;
		mine.printTab();
		System.out.println();
		mine.printMineTab();
		System.out.println();
		mine.revealEmpties(2, 2);
		mine.printRevealTab();
	}
}
