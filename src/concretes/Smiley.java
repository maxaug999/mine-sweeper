package concretes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class Smiley extends JPanel {

	public boolean dead = false;
	public boolean happiness = true;
	private int x;
	private int y;

	public Smiley(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void sad() {

	}

	public void smile() {

	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillOval(x, y, 35, 35);
		g.setColor(Color.BLACK);
		if (this.dead == true) {
			Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(2));
            g2.drawLine(x + 7, y + 10, x + 14, y + 17);
            g2.drawLine(x + 14, y + 10, x + 7, y + 17);
   
            g.fillRect(x + 10, y + 22, 15, 3);
			g.fillRect(x + 8, y + 25, 3, 3);
			g.fillRect(x + 24, y + 25, 3, 3);
		} else {
			g.fillOval(x + 7, y + 10, 5, 5);
			g.fillOval(x + 22, y + 10, 5, 5);
			if (this.happiness == true) {
				g.fillRect(x + 10, y + 25, 15, 3);
				g.fillRect(x + 8, y + 22, 3, 3);
				g.fillRect(x + 24, y + 22, 3, 3);
			} else {
				g.fillRect(x + 10, y + 22, 15, 3);
				g.fillRect(x + 8, y + 25, 3, 3);
				g.fillRect(x + 24, y + 25, 3, 3);
			}
		}
	}
}
