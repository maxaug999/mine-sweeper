package concretes;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

import concretes.GUI.Board;
import concretes.GUI.Click;
import concretes.GUI.Move;

public class welcomeView extends JFrame {
	private int mx;
	private int my;

	public welcomeView() {
		this.setTitle("Minesweeper");
		this.setSize(426, 375);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setLocation(400, 150);

		// Board board = new Board();
		// this.setContentPane(board);

		Move move = new Move();
		this.addMouseMotionListener(move);

		Click click = new Click();
		this.addMouseListener(click);
	}

	public class Move implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseMoved(MouseEvent e) {
			mx = e.getX();
			my = e.getY(); // Modifie en continue x et y
			/*
			 * System.out.println("x : " + mx + " y : " + my);
			 */
		}
	}

	public class Click implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}
}