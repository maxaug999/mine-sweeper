package concretes;

public class MineSweeper {
	public int lignes;
	public int col;
	public int[][] tab;
	public int[][] tabMine;
	public boolean[][] tabFlagged;
	public boolean[][] tabRevealed;
	private boolean victory;

	public MineSweeper(int lignes, int col) {
		this.lignes = lignes;
		this.col = col;
		tab = new int[lignes][col];
		tabMine = new int[lignes][col];
		tabFlagged = new boolean[lignes][col];
		tabRevealed = new boolean[lignes][col];
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				tabRevealed[i][j] = false;
				tabFlagged[i][j] = false;
			}
		}
	}
	
	public boolean determineIfVictorious() {
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				if(tabMine[i][j] != 1) {
					if(tabRevealed[i][j] != true) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public boolean isVictorious() { return this.victory;}
	
	public void revealEmpties(int i, int j) {
		if(tab[i][j] == 0 && tabRevealed[i][j] == true) {
			this.revealEmptiesRecursive(i, j);
		}
	}
	
	public void revealEmptiesRecursive(int i, int j) {
		try {
			if (tab[i - 1][j] == 0 && tabMine[i - 1][j] != 1 && tabRevealed[i -1][j] == false) {
				tabRevealed[i-1][j] = true;
				revealEmptiesRecursive(i-1,j);
			}else{revealNextToEmptiesRecursive(i,j);}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i][j - 1] == 0 && tabMine[i][j - 1] != 1 && tabRevealed[i][j - 1] == false) {
				tabRevealed[i][j-1] = true;
				revealEmptiesRecursive(i,j-1);
			}else{revealNextToEmptiesRecursive(i,j);}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i][j + 1] == 0 && tabMine[i][j + 1] != 1 && tabRevealed[i][j + 1] == false) {
				tabRevealed[i][j+1] = true;
				revealEmptiesRecursive(i,j+1);
			}else{revealNextToEmptiesRecursive(i,j);}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i + 1][j] == 0 && tabMine[i + 1][j] != 1 && tabRevealed[i + 1][j] == false) {
				tabRevealed[i+1][j] = true;
				revealEmptiesRecursive(i+1,j);
			}else{revealNextToEmptiesRecursive(i,j);}
		}catch(ArrayIndexOutOfBoundsException e) {}
	}
	
	public void revealNextToEmptiesRecursive(int i, int j) {
		try {
			if (tabMine[i - 1][j] != 1) {
				tabRevealed[i-1][j] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i - 1][j - 1] > 0 && tabMine[i - 1][j - 1] != 1 && tabRevealed[i -1][j - 1] == false) {
				tabRevealed[i-1][j - 1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i - 1][j + 1] > 0 && tabMine[i - 1][j + 1] != 1 && tabRevealed[i -1][j + 1] == false) {
				tabRevealed[i-1][j + 1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i][j - 1] > 0 && tabMine[i][j - 1] != 1 && tabRevealed[i][j - 1] == false) {
				tabRevealed[i][j-1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i][j + 1] > 0 && tabMine[i][j + 1] != 1 && tabRevealed[i][j + 1] == false) {
				tabRevealed[i][j+1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i + 1][j] > 0 && tabMine[i + 1][j] != 1 && tabRevealed[i + 1][j] == false) {
				tabRevealed[i+1][j] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i + 1][j - 1] > 0 && tabMine[i + 1][j - 1] != 1 && tabRevealed[i + 1][j - 1] == false) {
				tabRevealed[i+1][j - 1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
		try {
			if (tab[i + 1][j + 1] > 0 && tabMine[i + 1][j + 1] != 1 && tabRevealed[i + 1][j + 1] == false) {
				tabRevealed[i+1][j + 1] = true;
			}
		}catch(ArrayIndexOutOfBoundsException e) {}
	}
	
	public void generateMinesLocation(int q) {
		int randomLig;
		int randomCol;
		for (int i = 0; i < q;) {
			randomLig = (int) Math.floor(Math.random() * Math.floor(lignes));
			randomCol = (int) Math.floor(Math.random() * Math.floor(col));
			if (tabMine[randomLig][randomCol] == 0) {
				tabMine[randomLig][randomCol] = 1;
				i++;
			}
		}
	}


	public void setBoard() {
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				if (tabMine[i][j] != 1) {
					try {
						if (tabMine[i - 1][j - 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i - 1][j] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i - 1][j + 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i][j - 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i][j + 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i + 1][j - 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i + 1][j] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
					try {
						if (tabMine[i + 1][j + 1] == 1) {
							tab[i][j]++;
						}
					}catch(ArrayIndexOutOfBoundsException e) {}
				}
			}
		}
	}

	public void printTab() {
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(tab[i][j]);
			}
			System.out.println();
		}
	}

	public void printMineTab() {
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(tabMine[i][j]);
			}
			System.out.println();
		}
	}
	
	public void printRevealTab() {
		for (int i = 0; i < lignes; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(tabRevealed[i][j]);
			}
			System.out.println();
		}
	}
}
