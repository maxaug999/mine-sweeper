package concretes;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	public final static int NUMBER_OF_LIGNS = 16;
	public final static int NUMBER_OF_COLS = 16;
	public final static int HARD_LIGNS = 18;
	public final static int HARD_COLS = 30;
	public final static int EASY_DIFFICULTY_MINES = 40;
	public final static int MEDIUM_DIFFICULTY_MINES = 60;
	public final static int HARD_DIFFICULTY_MINES = 80;
	public final static int MEDIUM_WIDTH_SCREEN = 326;
	public final static int MEDIUM_WIDTH_HEIGHT = 375;
	private int smileyX = 142;
	private int smileyY = 2;
	private int smileyCenterX = smileyX + 35;
	private int smileyCenterY = smileyY + 35;
	private Smiley smiley = new Smiley(smileyX, smileyY);
	private boolean victory;
	private boolean defeat = false;
	private Date startDate = new Date();
	private int spacing = 1;
	private int mx;
	private int my;
	private MineSweeper mine;
	private long sec = 0;

	public GUI() {
		this.setTitle("Minesweeper");
		this.setSize(326, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setLocation(400, 150);

		Board board = new Board();
		this.setContentPane(board);

		Move move = new Move();
		this.addMouseMotionListener(move);

		Click click = new Click();
		this.addMouseListener(click);

		this.mine = new MineSweeper(NUMBER_OF_LIGNS, NUMBER_OF_COLS);
		mine.generateMinesLocation(40);
		mine.setBoard();
	}

	public class Board extends JPanel {
		@Override
		public void paintComponent(Graphics g) {
			g.setColor(Color.DARK_GRAY);
			g.fillRect(0, 0, 980, 680);
			smiley.paintComponent(g);
			for (int i = 0; i < NUMBER_OF_LIGNS; i++) {
				for (int j = 0; j < NUMBER_OF_COLS; j++) {
					g.setColor(Color.gray);
					/*
					if (mine.tabMine[i][j] == 1) { // ********* VISIBILITY OF MINES *******
						g.setColor(Color.yellow);
					}*/
					if (mine.tabRevealed[i][j] == true) {
						g.setColor(Color.white);
					}
					if (mx >= spacing + i * 20 + 2 && mx < spacing + i * 20 + 22 - 2 * spacing
							&& my >= spacing + 20 * j + 64 && my < spacing + 20 * j + 84 - 2 * spacing) {
						if (mine.tabRevealed[i][j] == false || mine.tab[i][j] > 0) {
							g.setColor(Color.LIGHT_GRAY);
						}
					}
					g.fillRect(spacing + 20 * i, spacing + 20 * j + 40, 20 - 2 * spacing, 20 - 2 * spacing);
					if (mine.tabRevealed[i][j] == true) {
						g.setColor(Color.BLACK);
						if (mine.tabMine[i][j] == 1) {
							// DEATH
							g.setColor(Color.BLACK);
							g.fillRect(i * 20 + 2 + 5, j * 20 + 40 + 5, 5, 10);
							g.fillRect(i * 20 + 5, j * 20 + 40 + 2 + 5, 10, 5);
							g.fillRect(i * 20 + 5 + 1, j * 20 + 40 + 1 + 5, 8, 8);
							// System.out.println("You died");
						} else if (mine.tab[i][j] != 0) {
							g.setFont(new Font("Tahoma", Font.BOLD, 15));
							if (mine.tab[i][j] == 1) {
								g.setColor(Color.blue);
							} else if (mine.tab[i][j] == 2) {
								g.setColor(Color.GREEN);
							} else if (mine.tab[i][j] > 2) {
								g.setColor(Color.RED);
							}
							g.drawString(String.valueOf(mine.tab[i][j]), i * 20 + 5, 20 * j + 40 + 16);
						}
					}

					// Flag painting
					if (mine.tabFlagged[i][j] == true && mine.tabRevealed[i][j] != true) {
						g.setColor(Color.BLACK);
						g.fillRect(i * 20 + 5, j * 20 + 42, 3, 14);
						g.setColor(Color.RED);
						g.fillRect(i * 20 + 5, j * 20 + 42, 8, 6);
					}
				}
			}

			// Time counter painting
			g.setColor(Color.BLACK);
			g.fillRect(240, 5, 60, 30);
			if (victory == false && defeat == false) {
				sec = (new Date().getTime() - startDate.getTime()) / 1000;
			}
			if (sec > 999) {
				sec = 999;
			}
			g.setFont(new Font("Tahoma", Font.BOLD, 50));
			if (defeat == true) {
				g.setColor(Color.RED);
				g.drawString("You lost !", 40, this.getHeight() / 2);
			} else if (victory == true) {
				g.setColor(Color.GREEN);
				g.drawString("You win !", 40, this.getHeight() / 2);
			} else {
				g.setColor(Color.WHITE);
			}
			g.setFont(new Font("Tahoma", Font.PLAIN, 28));
			if (sec < 10) {
				g.drawString("00" + String.valueOf(sec), 247, 30);
			} else if (sec < 100) {
				g.drawString("0" + String.valueOf(sec), 247, 30);
			} else {
				g.drawString(String.valueOf(sec), 247, 30);
			}
		}
	}

	public class Move implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseMoved(MouseEvent e) {
			mx = e.getX();
			my = e.getY(); // Modifie en continue x et y
			// System.out.println("x : " + mx + " y : " + my);
		}
	}

	public class Click implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			int x = inBoxX();
			int y = inBoxY();
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (isInSmiley()) { // *************** RESET BUTTON **********************
					startDate = new Date();
					smiley.dead = false;
					smiley.happiness = true;
					mine = new MineSweeper(NUMBER_OF_LIGNS, NUMBER_OF_COLS);
					mine.generateMinesLocation(50);
					mine.setBoard();
					sec = 0;
					victory = false;
					defeat = false;
					System.out.println("Pointer is in smiley");
				}
				if (x != -1 && y != -1 && defeat != true && victory != true) {
					if (mine.tabMine[x][y] == 1) {
						victory = false;
						defeat = true;
						smiley.dead = true;
						System.out.println("You hit a mine");
					}
					if (mine.tab[x][y] > 2) {
						smiley.happiness = false;
					} else {
						smiley.happiness = true;
					}
					mine.tabRevealed[x][y] = true;
					if (mine.tab[x][y] == 0) {
						mine.revealEmpties(x, y);
					}
					victory = mine.determineIfVictorious();
					if (victory == true) {
						System.out.println("Bravo champion ta gagn� !!");
					}
					// mine.tabMine[inBoxX()][inBoxY()] = 1;
					//System.out.println("The mouse is in box [" + x + "," + y + "]");
				}
			} else {
				if (x != -1 && y != -1 && defeat != true && victory != true) {
					if (mine.tabFlagged[x][y] == true) {
						mine.tabFlagged[x][y] = false;
					} else {
						mine.tabFlagged[x][y] = true;
					}
					System.out.println(e.getLocationOnScreen());
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	public int inBoxX() {
		for (int i = 0; i < NUMBER_OF_LIGNS; i++) {
			for (int j = 0; j < NUMBER_OF_COLS; j++) {
				if (mx >= spacing + i * 20 + 2 && mx < spacing + i * 20 + 22 - 2 * spacing
						&& my >= spacing + 20 * j + 64 && my < spacing + 20 * j + 84 - 2 * spacing) {
					return i;
				}
			}
		}
		return -1;
	}

	public int inBoxY() {
		for (int i = 0; i < NUMBER_OF_LIGNS; i++) {
			for (int j = 0; j < NUMBER_OF_COLS; j++) {
				if (mx >= spacing + i * 20 + 2 && mx < spacing + i * 20 + 2 + 20 - 2 * spacing
						&& my >= spacing + 20 * j + 20 + 44 && my < spacing + 20 * j + 20 + 44 + 20 - 2 * spacing) {
					return j;
				}
			}
		}
		return -1;
	}

	public boolean isInSmiley() {
		int dif = (int) Math.sqrt(Math.abs(mx - smileyCenterX) * Math.abs(mx - smileyCenterX)
				+ Math.abs(my - smileyCenterY) * Math.abs(my - smileyCenterY));
		if (dif < 35) {
			return true;
		}
		return false;
	}

	public boolean isVictorious() {
		return this.victory;
	}
}
